package com.sceballosdev.backend.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "connections")
public class ConnectionModel {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;
	
	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd")
    @ApiModelProperty(notes = "connection date of the User")
    @Column(name = "date_connection")
    String date_connection;
  
	
	@JsonFormat(shape = Shape.STRING, pattern = "HH:mm:ss")
    @ApiModelProperty(notes = "connection time of the User")
    @Column(name = "time_connection")
    String time_connection;
    
    @ApiModelProperty(notes = "id of the User")
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "user_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnore
    private User user;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public String getDate_connection() {
		return date_connection;
	}

	public void setDate_connection(String date_connection) {
		this.date_connection = date_connection;
	}

	public String getTime_connection() {
		return time_connection;
	}

	public void setTime_connection(String time_connection) {
		this.time_connection = time_connection;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
    
    


}
