package com.sceballosdev.backend.models;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

@Entity
@Table(name = "users",uniqueConstraints={@UniqueConstraint(columnNames={"email"})})
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @ApiModelProperty(notes = "name of the User")
    @Column(name = "username")
    private String username;

    @ApiModelProperty(notes = "email of the User")
    @Column(name = "email")
    private String email;

    @ApiModelProperty(notes = "password of the User")
    @Column(name = "password")
    private String password;
    
    @ApiModelProperty(notes = "full_name of the User")
    @Column(name = "full_name")
    private String full_name;
           
    @ApiModelProperty(notes = "Token for security requests")
    @Column(name = "jwt",columnDefinition = "varchar(500) default 'used to temporarily store jwt'")
    private String jwt;

    /*@OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private List<Location> locations;*/
       
   
	public User() {
	}
    
    public User(long id, String username, String email, String password, String full_name, String jwt) {
		
		this.id = id;
		this.username = username;
		this.email = email;
		this.password = password;
		this.full_name = full_name;
		this.jwt = jwt;
	}

	public long getId() {
        return id;
    }

	public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }    
    
       
	public String getFull_name() {
		return full_name;
	}

	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}

	public String getJwt() {
        return jwt;
    }    
    
    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    /*public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }*/
}
