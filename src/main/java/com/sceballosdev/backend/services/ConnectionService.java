package com.sceballosdev.backend.services;


import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.sceballosdev.backend.models.ConnectionModel;
import com.sceballosdev.backend.repository.ConnectionRepository;

@Service
public class ConnectionService {
	
	private final
    ConnectionRepository connectionRepository;
	
	public ConnectionService(ConnectionRepository connectionRepository) {
        this.connectionRepository = connectionRepository;
    }

    public List<ConnectionModel> getAllconnections() {
        List<ConnectionModel> connections = new ArrayList<>();
        connectionRepository.findAll().forEach(connections::add);
        return connections;
    }

    public ConnectionModel getconnectionById(long id) {
        return connectionRepository.findById(id).get();
    }

  
    public void saveOrUpdate(ConnectionModel connection) {
    	connectionRepository.save(connection);
    }

    public void delete(long id) {
    	connectionRepository.deleteById(id);
    }

}
