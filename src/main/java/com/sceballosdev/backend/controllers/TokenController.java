package com.sceballosdev.backend.controllers;

import com.sceballosdev.backend.models.User;
import com.sceballosdev.backend.security.JwtGenerator;
import com.sceballosdev.backend.services.UserService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/register")
public class TokenController {

    /**
     * The JPA repository
     */
    private final UserService userService;

    public TokenController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public User generate(@RequestBody final User jwtUser) {

        String token = JwtGenerator.generate(jwtUser);
        jwtUser.setJwt("used to temporarily store jwt");
        userService.saveOrUpdate(jwtUser);
        return userService.getUserByUsername(jwtUser.getUsername());

        // Guardar en la base de datos la información y el token del usuario
    }
}