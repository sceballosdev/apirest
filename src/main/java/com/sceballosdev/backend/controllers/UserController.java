package com.sceballosdev.backend.controllers;

import com.sceballosdev.backend.models.User;
import com.sceballosdev.backend.security.JwtGenerator;
import com.sceballosdev.backend.services.UserService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/users")
@Api(value = "User Resource REST Endpoint", description = "Shows the user info")
public class UserController {

    /**
     * The JPA repository
     */
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }
    
   

    /**
     * Used to fetch all the users from DB
     *
     * @return list of {@link User}
     */
    @GetMapping()
    public List<User> findAll() {
        return userService.getAllUsers();
    }

    /**
     * Used to find and return a user by id
     *
     * @param id refers to the name of the user
     * @return {@link User} object
     */
    @GetMapping(value = "/byId/{id}")
    public User findById(@PathVariable(value = "id") long id) {
        System.out.println("llega al end point a obtener usuario "+id);
        return userService.getUserById(id);
    }

    /**
     * Used to find and return a user by name
     *
     * @param username refers to the name of the user
     * @return {@link User} object
     */
    @GetMapping(value = "/byName/{username}")
    public User findByName(@PathVariable final String username) {
        return userService.getUserByUsername(username);
    }

    /**
     * Used to delete a user by id
     *
     * @param id refers to the name of the user
     * @return {@link User} object
     */
    @DeleteMapping(value = "/delete/{id}")
    public void deleteById(@PathVariable final long id) {
        userService.delete(id);
    }


    /**
     * Used to create a User in the DB
     *
     * @param users refers to the User needs to be saved
     * @return the {@link User} created
     */
    @PostMapping(value = "/save")
    public User saveUser(@RequestBody final User users) {
        userService.saveOrUpdate(users);
        return userService.getUserByUsername(users.getUsername());
    }
    
    /**
     * Used to login a User in the DB
     *
     * @param users refers to the User needs to be saved
     * @return the {@link User} created
     */
    @PostMapping(value = "/login")
    public User login(@RequestBody final User user) {
    	User u= userService.getUserByEmailAndPassword(user.getEmail(),user.getPassword());
    	if (u==null) {
			return noFound();
		}
    	u.setJwt(JwtGenerator.generate(user));
    	return u;   
    }
    
    
   public User noFound() {
	   return new User(0,"noFound", "noFound", "noFound","noFound", "noFound");
   }

}
