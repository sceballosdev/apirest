package com.sceballosdev.backend.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sceballosdev.backend.models.User;
import com.sceballosdev.backend.security.JwtGenerator;
import com.sceballosdev.backend.services.UserService;

import io.swagger.annotations.Api;

@RestController
@RequestMapping("/login")
@Api(value = "Login Resource REST Endpoint", description = "Shows the user info")
public class LoginController {
	
	
	  /**
     * The JPA repository
     */
    private final UserService userService;

    public LoginController(UserService userService) {
        this.userService = userService;
    }
	
    @GetMapping
    public String login() {
        return "Login Start";
    }
    
    
    /**
     * Used to login a User in the DB
     *
     * @param user refers to the User needs to be saved
     * @return the {@link User} created
     */
    @PostMapping
    public User login(@RequestBody final User user) {
    	User u= userService.getUserByEmailAndPassword(user.getEmail(),user.getPassword());
    	if (u==null) {
			return noFound();
		}
    	u.setJwt(JwtGenerator.generate(user));
    	System.out.println(u.getUsername());
    	return u;   
    }
    
    
   public User noFound() {
	   return new User(0,"noFound", "noFound", "noFound","noFound", "noFound");
   }
}
