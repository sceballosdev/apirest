package com.sceballosdev.backend.controllers;

import com.sceballosdev.backend.models.Greeting;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

@RestController
@RequestMapping("/greeting")
@Api(value = "Greeting Resource", description = "shows a greeting")
public class GreetingController {
    private static final String TEMPLATE = "Hi, %s!";
    private final AtomicLong counter = new AtomicLong();

    @ApiOperation(value = "Returns Hello World")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 100, message = "100 is the message"),
                    @ApiResponse(code = 200, message = "Successful Hello World")
            }
    )

    @GetMapping()
    public Greeting greeting(@RequestParam(value = "name", defaultValue = "World UNIAJC") String name) {
        return new Greeting(counter.incrementAndGet(), String.format(TEMPLATE, name));
    }
}
