package com.sceballosdev.backend.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.sceballosdev.backend.models.ConnectionModel;
import com.sceballosdev.backend.models.User;
import com.sceballosdev.backend.services.ConnectionService;
import com.sceballosdev.backend.services.UserService;

import io.swagger.annotations.Api;

@RestController
@RequestMapping("/rest/connections")
@Api(value = "Connection Resource REST Endpoint", description = "Shows the connection info")
public class ConnectionController {
	
	 /**
     * The JPA repository
     */
    private final UserService userService;
    private final ConnectionService connectionService;

    public ConnectionController(ConnectionService connectionService, UserService userService) {
        this.connectionService = connectionService;
        this.userService = userService;
    }

    /**
     * Used to fetch all the connections from DB
     *
     * @return list of {@link connection}
     */
    @GetMapping()
    public List<ConnectionModel> findAll() {
        return connectionService.getAllconnections();
    }

    /**
     * Used to find and return a connection by id
     *
     * @param id refers to the name of the connection
     * @return {@link connection} object
     */
    @GetMapping(value = "/{id}")
    public ConnectionModel findById(@PathVariable final long id) {
        return connectionService.getconnectionById(id);
    }


    /**
     * Used to delete a connection by id
     *
     * @param id refers to the name of the connection
     * @return {@link connection} object
     */
    @DeleteMapping(value = "/{id}")
    public void deleteById(@PathVariable final long id) {
        connectionService.delete(id);
    }


    /**
     * Used to create a connection in the DB
     *
     * @param user_id   refers to the connection needs to be saved
     * @param connections refers to the connection needs to be saved
     * @return the {@link connection} created
     */
    @PostMapping(value = "/{user_id}")
    public ConnectionModel saveconnection(@PathVariable(value = "user_id") Long user_id,
                                 @Valid @RequestBody final ConnectionModel connections) {

        User currentUser = userService.getUserById(user_id);
        connections.setUser(currentUser);
        connectionService.saveOrUpdate(connections);
        return connectionService.getconnectionById(connections.getId());
    }

}
