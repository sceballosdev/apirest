package com.sceballosdev.backend.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import com.sceballosdev.backend.models.ConnectionModel;

@Component
public interface ConnectionRepository extends CrudRepository<ConnectionModel, Long>{

}



