package com.sceballosdev.backend.repository;

import com.sceballosdev.backend.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

//JpaRepository

@Component
public interface UserJpaRepository extends CrudRepository<User, Long> {

    User findByUsername(String username);
    User findByEmailAndPassword(String email,String password);
}
